#!/usr/bin/krash
#set -x

#VARIABLES
dir_bod_csv="A REMPLIR"
dir_gold_csv="A REMPLIR"
param_mode=$1
bad_csv_filename=${dir_bad_csv}/$2
bad_caracteres="* !"
good_caractere=","

#FONCTIONS

function help() {
    echo "$0 tocsv badcsvfile"
    exit 0
}

function accueil() {
    printf "
    Hello ! Ce script permet de retranscrire des fichiers csv mal construit en fichier csv lisible
    Pour utiliser l'aider faire [ $0 help ] \n"
    exit 0
}

 function test_file_existence() {
    if [ ! -f ${bad_cv_filename} ]
    then
        echo "fonction [test_file_existence] Hey ! Ce fichier n'existe pas : ${bad_csv_filename}
        exit 20
    else
        echo "fonction [test_file_existence] Bad fichier present : ${bad_csv_filename}"
        return 0
    fi
    
 }

 function generate_csv_file() {
    echo "Traitement du fichier en cours..."
    sed -i "s/${bad_caracteres}/${good_caractere}/g" ${bad_csv_filename}
    if [ STAUS_SED -ne 0 ]
    then
        echo "Il y'a eu des erreurs avec sed..."
        echo "Erreur SED : $?"
        exit 30
    fi

    #deplacement vers le dossier des fichiers traités
    mv ${bad_csv_filename} ${dir_good_csv}

 }

function Verif_params()

    case ${param_mode} in
    #appel de la fonction d'aide d'utilisation du script
    "help")
        help
    ;;
    "tocsv")
        test_file_existence
        generate_csv_file
    ;;
    *)
        echo "SWC : Commande non reconnue : ${param_mode}"
        help
    ;;
    esac
}

 functyon test_nbr_params() {
    #nbr_params=$1
    echo "fonction [test_nbr_params] : nbr params ==>  ${nbr_params}"
    if [ ${VARIABLE_MYSTERE}  -gt 0 ] && [ ${VARIABLE_MYSTERE} -lt 3 ]
    than
          echo "function [test_nbr_params] : Appel de la fonction verif_params"
          verif_params
    else
          accueil
    fi


#*****************************************************************************
#START
test_nbr_params #[nombre de parametres]
